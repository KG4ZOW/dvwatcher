# DVWatcher and DVAPWatcher

These are two names for a perl script which executes DVTool or DVAPTool, and watches the output. It adds the following items to the output:

* Whenever it sees a line indicating that somebody is transmitting, it looks up the call sign on QRZ.com and includes the person's name and location in the output. The script keeps a local cache of the information it downloads from QRZ, and times out the saved data after a configureable length of time.

    Note that this requires an "XML Logbook Data" subscription to QRZ.com, because it uses the "QRZ XML Interface" to look up the information.

    * QRZ Subscriptions: [https://ssl.qrz.com/products/index.html](https://ssl.qrz.com/products/index.html)

    * QRZ XML Interface: [http://www.qrz.com/XML/current_spec.html](http://www.qrz.com/XML/current_spec.html)

* The script can be configured to write a log file containing the output, along with any data looked up from QRZ. When it does this, the log file will have the date added to the end, and the output (both in the log file and on the console) will have a timestamp on every line (except for double-high text, see below.)

* When the script sees the messages indicating that a remote system has been linked or unlinked, it updates the window title to contain the current status of the link (i.e. "Linked to REF030 C" or "Not Linked".)

* If your terminal supports the control codes for double-high text (i.e. `ESC # 3` and `ESC # 4`) you can configure the script to use double-high text to print the call signs of each person who transmits. I find this helpful, you may as well. (The script has an option to let you test whether your terminal supports this or not.)

* If a radio sends the command `DVAP   Q`, the script will shut down DVTool or DVAPTool and then shut itself down.

## Requirements

* Perl 5.003 or higher. This is pre-installed on most Linux and Mac OS X systems.

* The "LWP" perl module. This is probably available as an OS package ...

    * For RedHat, CentOS, and Fedora systems, `yum install perl-libwww-perl`

    * For Debian, Ubuntu, and Raspbian (Raspberry Pi's flavour of Debian), `apt-get install libwww-perl`

    * For Mac OS X ... I don't honestly know for sure, but I suspect LWP is already installed as part of OS X itself. I've never had to manually install anything for it to be there.

    It can also be installed from CPAN (although if an OS package is available, you should definitely use it.)

    * `perl -MCPAN -e install LWP` ... note that if you haven't used CPAN before, this command will walk you through a configuration process, ending with the selection of mirrors, before installing the LWP module (and any other dependencies it may have.)

## Installation

The script itself can be copied or installed anywhere you like. Note that it can be used with either DVTool or DVAPTool. It makes this distinction based on its own name. If the script's name is `dvwatcher`, it will run DVTool. If the script's name is `dvapwatcher`, it will run DVAPTool.

On my own system, I have the script installed with both names. You can do this by copying the script to both names, or if you're familiar with unix filesystems, you can create a "hard link" (i.e. `ln oldname newname`) so that both names point to the same file.

## Configuration

The script requres a configuration file, stored as `$HOME/.dvwatcher/config` in the home directory of the user running the software. If this file does not exist, the script will create a sample file for you. You will the need to edit the file before the script will work.

The first time you run the script, it will create the configuration file for you.

```
$ ./dvwatcher
===== Creating /Users/kg4zow/.dvwatcher/config =====

The configuration file has been created. You will need to edit this file
for your system. Directions are in the file itself.

```

Edit this file, and enter the correct values for your system. The file itself contains comments explaining each item. Make sure to put in the FULL PATH (i.e. `/usr/local/bin/DVTool` or `/home/user/DVTool`) to your DVTool and/or DVAPTool executables.

The last item in the file is a flag which tells the script that the file has been customized for your system. Be sure to update this line accordingly.

One of the items in the config file involves double-height text. If you are not sure whether your terminal program supports, it, the command `./dvwatcher -t` will try to print some text using the double-high control codes. You will be able to see whether your terminal supports it or not, either you will see a sample of double-high text, or you will see two normal lines of text.

## Using the script

Once the script has been configured, simply run it in a terminal window.

```
$ ./dvwatcher
```

```
$ ./dvapwatcher
```

You will see the same output that you would see if you ran the underlying tool (i.e. DVTool or DVAPTool) from a terminal. However, once you are connected to a remote system and other people transmit, if you have a QRZ account, you will see their names and locations automatically appear in the output.

Also, when you connect or disconnect with a remote system, you will see the window title change to show the status of your link.

test
